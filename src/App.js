import React, { Component } from 'react'
import DataTable from './DataTable'
import 'jquery/dist/jquery.min.js';
import 'popper.js/dist/popper.min.js';
import 'bootstrap/dist/js/bootstrap.min.js';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css'

class App extends Component {
  render() {
    return (
      <div className="App">
        {/*<div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>Welcome to React</h2>
        </div>*/}
        {/*<p className="App-intro">*/} {/*</p>*/}
        <h2>Table with data</h2>
        {/*<Table data={data}/>
        {To get started, edit <code>src/App.js</code> and save to reload.*/}
        <DataTable />
      </div>
    );
  }
}

export default App;