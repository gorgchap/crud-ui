import React, { Component } from 'react'
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table'
import '../node_modules/react-bootstrap-table/css/react-bootstrap-table.css'
import axios from 'axios'
import Modal from './Modal'

class Table extends Component {
  constructor(props) {
    super(props); this.addRow = this.addRow.bind(this); this.editRow = this.editRow.bind(this);
    this.state = { error: null, isLoading: true, items: [], count: 0, sel: null };
  }
  
  componentDidMount() {
    axios.get('http://178.128.196.163:3000/api/records')
      .then(
        result => { this.setState(st => ({ isLoading: false, items: result.data, count: result.data.length })); },
        error => { this.setState(st => ({ isLoading: false, error })); }
      )
  }
  
  addRow(row) {
    if (!row['data']) { alert('Empty textfield!'); return false; }
    const fn = row['data'].split(' '); if (fn.length > 2 || fn[0] === "" || fn[1] === "") { 
      alert('Full Name does not consist of one space separated name and surname or only name!'); return false; }
    const msg = JSON.parse('{"data": {"name": "' + fn[0] + (fn.length === 1 ? '"}}' : '", "surname": "' + fn[1] + '"}}'));
    axios.put('http://178.128.196.163:3000/api/records', msg)
      .then(
        response => { this.setState(st => ({ isLoading: false, items: st.items.concat(response.data), count: st.count + 1 })); },
        error => { this.setState(st => ({ isLoading: false, error })); }    
      )
  }
  
  deleteRow(id) {
    axios.delete('http://178.128.196.163:3000/api/records/' + id)
      .then(
        result => { this.setState(st => ({ isLoading: false, items: st.items.filter(item => item['_id'] !== id), count: st.count - 1 })); },
        error => { this.setState(st => ({ isLoading: false, error })); }
      )
  }
  
  editRow(row) {
    if (row['name'] === '' || row['__v'] === '') { alert('Name and __v must be filled!'); return false; }
    if (row['name'].indexOf(' ') > -1) { alert('Spaces in name are prohibited!'); return false; }
    if (!row['surname']) row['surname'] = '';
    if (row['surname'] !== '' && row['surname'].indexOf(' ') > -1) { alert('Spaces in surname are prohibited!'); return false; }
    const surname = row['surname'] === '' ? '' : '"surname":"' + row['surname'] + '", '; 
    const __v = Number(row['__v']); if (isNaN(__v) || __v < 0) { alert('__v is not a non-negative number!'); return false; }
    const msg = JSON.parse('{"data":{' + surname + '"name":"' + row['name'] + '"},"__v":' + __v + '}');
    axios.post('http://178.128.196.163:3000/api/records/' + row['_id'], msg)
      .then(
        response => {
          axios.get('http://178.128.196.163:3000/api/records/' + row['_id'])
            .then(
              result => { let temp = this.state.items; temp[temp.findIndex(x => x['_id'] === result.data['_id'])] = result.data; this.setState({ items: temp }); },
              error => { this.setState(st => ({ isLoading: false, error })); }    
            )
        },
        error => { this.setState(st => ({ isLoading: false, error })); }    
      )
  }

  render() {
    const dataFormatter = (cell, row) => cell['name'] + (cell['surname'] ? ' ' + cell['surname'] : '')
    const buttonsFormatter = (cell, row) => (
      <div>
        <button className="btn btn-primary" data-toggle="modal" data-target="#exampleModal" onClick={() => this.setState({ sel: row })}>Edit</button> {' '}
        <button className="btn btn-danger" onClick={() => this.deleteRow(row['_id'])}>Delete</button>
      </div>
    )
    const showTotal = () => <div>There are {this.state.count} items total</div>
    const options = {
      prePage: '<', nextPage: '>', firstPage: '<<', lastPage: '>>',
      onAddRow: this.addRow, paginationShowsTotal: showTotal
    }
    const { error, isLoading, items, sel } = this.state;
    if (error) return <div><h3 style={{color: 'red'}}>Error: {error.message}</h3></div>; 
    else if (isLoading) return <div><h3 style={{color: 'green'}}>Loading...</h3></div>;
    else return (
      <div>
        <BootstrapTable data={items} options={options} insertRow pagination hover>
          <TableHeaderColumn isKey hiddenOnInsert dataField='_id' dataAlign='center' headerAlign='center'>
            Id
          </TableHeaderColumn>
          <TableHeaderColumn dataField='data' dataFormat={dataFormatter} dataAlign='center' headerAlign='center'>
            Full Name
          </TableHeaderColumn>
          <TableHeaderColumn dataField='__v' hiddenOnInsert dataAlign='center' headerAlign='center'>
            __v
          </TableHeaderColumn>
          <TableHeaderColumn dataFormat={buttonsFormatter} dataAlign='center' headerAlign='center'>
            Actions
          </TableHeaderColumn>
        </BootstrapTable>
        { sel ? <Modal _id={sel['_id']} name={sel['data']['name']} surname={sel['data']['surname']} __v={sel['__v']} editRow={this.editRow}/> : null }
      </div>
    );
  }
}
 
export default Table;