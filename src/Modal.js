import React, { Component } from 'react';

class Modal extends Component {
  constructor(props) { super(props); this.state = { _id: '', name: '', surname: '', __v: -1 } }

  componentWillReceiveProps(next) { this.setState({ _id: next._id, name: next.name, surname: next.surname, __v: next.__v }); }
  
  render() {
    return (
      <div className="modal fade" id="exampleModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="exampleModalLabel">Edit {this.state._id}</h5>
              <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              <p>
                <span className="modal-lable">Name: </span>
                <input value={this.state.name} onChange={e => this.setState({ name: e.target.value })} />
              </p>
              <p>
                <span className="modal-lable">Surname: </span>
                <input value={this.state.surname ? this.state.surname : ""} onChange={e => this.setState({ surname: e.target.value })} />
              </p>
              <p>
                <span className="modal-lable">__v: </span>
                <input value={this.state.__v} onChange={e => this.setState({ __v: e.target.value })} />
              </p>
            </div>
            <div className="modal-footer">
              <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="button" className="btn btn-primary" data-dismiss="modal" onClick={() => { this.props.editRow(this.state) }}>Save changes</button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Modal;