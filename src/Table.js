import React, { Component } from 'react'
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table'
import '../node_modules/react-bootstrap-table/css/react-bootstrap-table.css'

class Table extends Component {
  constructor(props) {
    super(props);
    this.addRow = this.addRow.bind(this);
    this.deleteRows = this.deleteRows.bind(this);
    this.showTotal = this.showTotal.bind(this);
    this.state = { items: props.data, count: props.data.length };
  }
  
  addRow(row) {
    if (!(row["title"] && row["year"] && row["role"] && row["director"])) { alert('There are empty textfields!'); return false; }
    const year = Number(row["year"]); if (isNaN(year)) { alert('Year is not numeric!'); return false; }
    if (year < 1) { alert('Year is not a positive value!'); return false; } row["year"] = year;
    var id = Math.round(0.5 + Math.random() * 1000);
    while (!function() { // eslint-disable-line no-loop-func
      for (let i = 0; i < this.state.count; i++)
        if (id === this.state.items[i]["id"])
          return false;
        return true;
    }) id++; row["id"] = id; 
    this.setState(st => ({ items: st.items.concat(row), count: st.count + 1 }));
  }
  
  deleteRows(ids) { this.setState(st => ({ items: st.items.filter(item => !ids.includes(item["id"])), count: st.count - ids.length })); }
  
  showTotal() { return <p>There are {this.state.count} items total</p> }
  
  render() {
    const options = {
      prePage: '<', nextPage: '>', firstPage: '<<', lastPage: '>>',
      onAddRow: this.addRow, onDeleteRow: this.deleteRows,
      paginationShowsTotal: this.showTotal
    }
    return (
      <div>
        <BootstrapTable data={this.state.items} options={options} insertRow deleteRow selectRow={{mode: 'checkbox'}} pagination hover>
          <TableHeaderColumn isKey dataField='id' hidden hiddenOnInsert dataAlign='center' headerAlign='center' width='50'>
            Id
          </TableHeaderColumn>
          <TableHeaderColumn dataField='title' dataAlign='center' headerAlign='center'>
            Title
          </TableHeaderColumn>
          <TableHeaderColumn dataField='year' dataAlign='center' headerAlign='center' width='100'>
            Year
          </TableHeaderColumn>
          <TableHeaderColumn dataField='role' dataAlign='center' headerAlign='center'>
            Role
          </TableHeaderColumn>
          <TableHeaderColumn dataField='director' dataAlign='center' headerAlign='center'>
            Director
          </TableHeaderColumn>
        </BootstrapTable>
      </div>
    );
  }
}
 
export default Table;